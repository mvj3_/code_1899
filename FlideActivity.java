package com.example.flidelistviewdemo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FlideActivity extends Activity {

	private ListView lv_flide;
	private ArrayList<String> data;
	private ListAdapter adapter;
	private LayoutInflater inflater = null;
	private Map<Integer,Boolean> dataMap;
	private View view;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		

		setContentView(R.layout.main);
		inflater = LayoutInflater.from(FlideActivity.this);
		lv_flide = (ListView) findViewById(R.id.listView1);
		data = new ArrayList<String>();
		for (int i = 0; i < 10; i++) {
			data.add("delete me ?  "+i);
		}
		dataMap  = new HashMap<Integer,Boolean>();
		for(int j = 0;j<data.size();j++)
		{
			dataMap.put(j, false);
		}
		adapter = new ListAdapter();
		lv_flide.setAdapter(adapter);
		
		lv_flide.setOnTouchListener(new OnTouchListener() {
			float oldX = 0;
			float oldY = 0;
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					//保存按下时的X,Y坐标
					oldX = event.getX();
					oldY = event.getY();
					break;
				case MotionEvent.ACTION_UP:
					//获取按起时的X,Y坐标
					float newX = event.getX();
					float newY = event.getY();
					//通过ListView中的pointToPosition方法获取点击ListView中的位置position
					final int oldPosition = ((ListView)v).pointToPosition((int)oldX, (int)oldY);
					int newPosition = ((ListView)v).pointToPosition((int)newX, (int)newY);
					
					if( newX - oldX > 20  && oldPosition == newPosition) {
						//获取ListView中点击是的View
						view = ((ListView)v).getChildAt(oldPosition);
						for(int n=0;n<data.size();n++)
						{
							if(n==oldPosition)
							{
								dataMap.put(n, true);
							}
							else
							{
								dataMap.put(n, false);
							}
						}
						adapter.notifyDataSetChanged();
					}
					break;
				default:
					break;
				}

				return false;
			}
		});
	}
	
	private void removeItemView(View v, final int position) {
		// TODO Auto-generated method stub
		//获取动画
		Animation animation = AnimationUtils.loadAnimation(v.getContext(), android.R.anim.slide_out_right);
		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			public void onAnimationRepeat(Animation animation) {
			}
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				//当动画完时移除数据
				data.remove(position);
				//更新ListView
				dataMap.put(position, false);
				adapter.notifyDataSetChanged();
			}
		});
		v.startAnimation(animation);
	}
	
	class ListAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			return data==null?0:data.size();
		}

		@Override
		public Object getItem(int arg0) {
			return data==null?null:data.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {			
			return arg0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup arg2) {						
			ViewHolder holder = null;
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.adapter_item, null);
				holder = new ViewHolder();
				holder.name = (TextView) convertView.findViewById(R.id.listItem_name);
				holder.delete = (Button) convertView.findViewById(R.id.listItem_delete);
				holder.rl =  (RelativeLayout) convertView.findViewById(R.id.listItem_rl);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			// 给控件赋值
			holder.name.setText(data.get(position));
			if(dataMap.get(position))
			{
				holder.delete.setVisibility(View.VISIBLE);
				holder.delete.setTextColor(Color.RED);
				//删除ListView指定项
				holder.delete.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View arg0) {
						removeItemView(view,position);
					}							
				});
			}
			else
			{
				holder.delete.setVisibility(View.INVISIBLE);
			}
			return convertView;
		}
		
	}
	
	private final class ViewHolder {
		public TextView name; //名称
		public Button delete;//删除按钮
		public RelativeLayout rl;
	}

}
